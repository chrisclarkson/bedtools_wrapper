#!/usr/bin/env python3

import sys
import os
import subprocess as sb
import argparse
parser = argparse.ArgumentParser()

parser.add_argument('a',
                    type=str,
                    help='-a bed file '
                    )

parser.add_argument('b',
                    type=str,
                    help='-a bed file '
                    )

parser.add_argument('--genome',
                    type=str,
                    help='genome version',
                    default='/home/rmgpccl/bedtools2/genomes/special')
parser.add_argument('--output',
                    type=str,
                    help='output counts',
                    default='counts')

parser.add_argument('--times',
                    type=int,
                    help='number of times to compare',
                    default=5)

args = parser.parse_args()

a=args.a
b=args.b
if args.genome:
    genome=args.genome
else:
    genome='/home/rmgpccl/bedtools2/genomes/special'

if args.output:
    output=args.output
else:
    output=None

if args.times:
    times=args.times
else:
    times=5


if output is not None:
    if not os.path.isfile(output):
        with open(output,'a') as out:
            out.write('A\tB\tenrichment\n')

# for i in range(times):
#     os.system('bedtools shuffle -i {0} -g {1} | bedtools sort -i stdin > {2}_randomised_{0}'.format(b,genome,str(i)))
#     random_count=os.popen('bedtools intersect -a {0} -b {2}_randomised_{1} | wc -l'.format(a,b,str(i))).read()
#     if i==0:
#         count=os.popen('bedtools intersect -a {0} -b {1} | wc -l'.format(a,b,str(i))).read()
#     with open(output,'a') as out:
#         out.write('{0}\t{1}\t{2}\n'.format(a.replace('.bed','').replace('.gz',''),count.rstrip(),random_count.rstrip(),str(i)))
#     #os.system('rm {1}_randomised_{0}'.format(b,str(i)))

lines=os.popen('bedtools fisher -a {0} -b {1} -g {2}'.format(a,b,genome)).readlines()
print(''.join(lines))
last_line=lines[-1].split('\t')[-1].rstrip()
print(last_line)
if output is not None:
    with open(output,'a') as out:
        out.write('{0}\t{1}\t{2}\n'.format(os.path.basename(a).replace('.bed','').replace('.gz',''),os.path.basename(b).replace('.bed','').replace('.gz',''),last_line))



# os.system('Rscript enrichment_plot.R {0}'.format(output))

