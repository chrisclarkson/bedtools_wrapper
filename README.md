
![](bedtools_wrapper_illustration.png?raw=true)


### Bedtools wrapper- for documenting enrichment results
e.g.
`python intersect_enrichment.py Brain_Hippocampus_filtered.bed Marzi_H3K27ac_EntorhinalCortex_38_ctd.bed`

```
# Number of query intervals: 240
# Number of db intervals: 174522
# Number of overlaps: 361
# Number of possible intervals (estimated): 227122
# phyper(361 - 1, 240, 227122 - 240, 174522, lower.tail=F)
# Contingency Table Of Counts
#_________________________________________
#           |  in -b       | not in -b    |
#     in -a | 361          | 0            |
# not in -a | 174161       | 52600        |
#_________________________________________
# p-values for fisher's exact test
left    right   two-tail        ratio
1       4.5798e-42      7.3576e-42      inf
```

A was infinitely enriched in B.

Let's loop through a range of combinations and document each result in a file.

```

while read f
do
    python intersect_enrichment.py Brain_Hippocampus_filtered.bed Marzi_H3K27ac_EntorhinalCortex_38_ctd.bed --output results.txt
done < tissues
```

We can then plot the results:
`Rscript enrichment_plot.R results.txt # will give rise to results.txt.pdf`
